﻿using UnityEngine;
using System.Collections;

public class PlayerSoundEmitter : SoundEmitter 
{
	[Header("Player shout")]
	public int numPulsesPerShout = 10;

	[Range (0.0f, 90.0f)]
	public float maxShoutFovDegrees = 45.0f;
	public float shoutSpeedMultIncrementBetweenPulses = 0.5f;

	void Start ()
    {
        base.Start();
	}
	
	void Update () 
    {
        base.Update();

		if (Input.GetMouseButtonDown(0))
        {
			EmitOmniDirectionalSound();
		}

		if (Input.GetMouseButtonDown(1))
		{
			EmitSound(numPulsesPerShout, maxShoutFovDegrees, shoutSpeedMultIncrementBetweenPulses);
		}
	}
}
