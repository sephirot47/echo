﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundEmitter : MonoBehaviour 
{  
    // Pulse related
    [Header("Pulse")]
    public float pulseLineTremble = 0.0f;
    public float pulseLineDistanceTremble = 0.0f;
    public float pulseRandomDir = 0.0f;
    public float pulseSpeed = 5.0f;
    public float fovSpeed = 0.001f;
    public float pulseMaxEmissionDistance = 999999.9f; 
	public int pulseRaysPerPulse = 10;
	public float pulseLifeTimeAfterCollided = 0.0f;

    public Color pulseColor1 = Color.blue;
    public Color pulseColor2 = Color.green;

    public GameObject prefabSoundPulse;
    protected List<SoundPulse> soundPulses;

    [Header("Emitter")]
    public int numPulses = 15;
	public bool snapPulsesToEmitter = false;
    public bool ignoreSelfCollisions = false;
    public bool seeLinesInAir = true;
    public Vector3 originOffset = Vector3.zero;

    protected void Start()
    {
        soundPulses = new List<SoundPulse>();
    }

    protected void Update ()
    {
        if (snapPulsesToEmitter)
        {
            SetEmissionOrigin(transform.position);
        }
    }

    public void EmitSinglePulse (float fov, float speedMultiplier, Vector3 dir, Color color)
    {
        GameObject go = GameObject.Instantiate(prefabSoundPulse) as GameObject;
        SoundPulse pulse = go.GetComponent<SoundPulse>();

        pulse.lineTremble = pulseLineTremble;
        pulse.lineDistanceTremble = pulseLineDistanceTremble;
        pulse.randomDir = pulseRandomDir;

        pulse.speed = pulseSpeed * speedMultiplier;
        pulse.fovSpeed = fovSpeed;

        pulse.maxEmissionDistance = pulseMaxEmissionDistance;
        pulse.raysPerPulse = pulseRaysPerPulse;

        pulse.lifeTimeAfterCollided = pulseLifeTimeAfterCollided;

        pulse.ignoreSelfCollisions = ignoreSelfCollisions;

        pulse.seeLinesInAir = seeLinesInAir;

        pulse.color = color;

        pulse.Init(fov, 
            originOffset + transform.position, 
            dir, 
            this);

        soundPulses.Add(pulse);
    }

    public void EmitSinglePulse (float fov, float speedMultiplier, Vector3 dir)
	{
        EmitSinglePulse(fov, speedMultiplier, dir, pulseColor1);
	}

	public void EmitOmniDirectionalSound ()
	{
		EmitSound (numPulses, 180.0f, 0.0f);
	}

	public void EmitSound (float _numPulses, float maxEmissionFovDegrees,
						   float emissionSpeedMultIncrementBetweenPulses, 
						   Vector3 dir)
	{
		float speedMult = 1.0f;
		float maxShoutFovRads = maxEmissionFovDegrees * Mathf.PI / 180.0f;
		float fovStep = maxShoutFovRads / _numPulses;
		for (float fov = fovStep; fov < maxShoutFovRads; fov += fovStep) 
		{
            Color degColor = Color.Lerp(pulseColor1, pulseColor2, fov / maxShoutFovRads);

			EmitSinglePulse (fov, speedMult, dir, degColor);
			speedMult += emissionSpeedMultIncrementBetweenPulses;
		}
	}

	public void EmitSound (float _numPulses, float maxEmissionFovDegrees = 180.0f,
						   float emissionSpeedMultIncrementBetweenPulses = 0.0f)
	{
		EmitSound (_numPulses, maxEmissionFovDegrees, emissionSpeedMultIncrementBetweenPulses, transform.forward);
    }

    public void SetEmissionOrigin (Vector3 position)
    {
        foreach (SoundPulse sp in soundPulses)
        {
            sp.SetEmissionOrigin(position);
        }
    }

    public List<SoundPulse> GetSoundPulses ()
    {
        return soundPulses;
    }

    public void RemovePulse (SoundPulse pulse)
    {
        soundPulses.Remove(pulse);
    }

    public void RemoveAllPulses ()
    {
        foreach (SoundPulse sp in soundPulses)
        {
            Destroy(sp.gameObject);
        }
        soundPulses.Clear();
	}

	protected void OnDrawGizmos ()
    {
        Gizmos.color = Color.Lerp(pulseColor1, pulseColor2, 0.5f);
        Gizmos.DrawIcon (transform.position, "speaker", true);
        Gizmos.DrawWireSphere (transform.position, pulseMaxEmissionDistance);
        Gizmos.DrawLine (transform.position, transform.position + transform.forward * pulseMaxEmissionDistance);
	}
}
