﻿using UnityEngine;
using System.Collections;

public class StepSoundEmitter : SoundEmitter 
{
	[Header("Step pulses")]
	public int numPulsesPerStep = 10;
	[Range (0.0f, 180.0f)] public float maxStepFovDegrees = 45.0f;
	public float stepSpeedMultIncrementBetweenPulses = 0.5f;
	public float stepRadius = 0.5f;
	public float stepSpeed = 0.5f;
    public Color stepColor1 = Color.white, stepColor2 = Color.black;

	[Header("Jump pulses")]
	public int numPulsesPerJump = 10;
	[Range (0.0f, 180.0f)] public float maxJumpFovDegrees = 45.0f;
	public float jumpSpeedMultIncrementBetweenPulses = 0.5f;
	public float jumpRadius = 0.5f;
	public float jumpSpeed = 0.5f;
    public Color jumpColor1 = Color.white, jumpColor2 = Color.black;

	[Header("Land pulses")]
	public int numPulsesPerLand = 10;
	[Range (0.0f, 180.0f)] public float maxLandFovDegrees = 45.0f;
	public float landSpeedMultIncrementBetweenPulses = 0.5f;
	public float landRadius = 0.5f;
	public float landSpeed = 0.5f;
    public Color landColor1 = Color.white, landColor2 = Color.black;

	[Header("Other")]
	public Vector3 floorOffset = new Vector3 (0.0f, -0.6f, 0.0f);

	public bool leftStep = true;

	void Start ()
    {
        base.Start();
	}
	
	void Update () 
    {
        base.Update();
    }

	void EmitStepSound()
	{
		originOffset = floorOffset;
		pulseMaxEmissionDistance = stepRadius;
		pulseSpeed = stepSpeed;

        pulseColor1 = stepColor1;
        pulseColor2 = stepColor2;
		if (leftStep)
		{
			originOffset +=  Vector3.right * 1.0f;
		} 
		else
		{
			originOffset += -Vector3.right * 1.0f;
		}
        
		EmitSound(numPulsesPerStep, maxStepFovDegrees, stepSpeedMultIncrementBetweenPulses, Vector3.up);
		leftStep = !leftStep;
	}

	void EmitJumpSound()
	{
		originOffset = floorOffset;
		pulseMaxEmissionDistance = jumpRadius;
		pulseSpeed = jumpSpeed;
        pulseColor1 = jumpColor1;
        pulseColor2 = jumpColor2;
		EmitSound(numPulsesPerJump, maxJumpFovDegrees, jumpSpeedMultIncrementBetweenPulses, Vector3.up);
	}

	void EmitLandSound()
	{
		originOffset = floorOffset;
		pulseMaxEmissionDistance = landRadius;
        pulseSpeed = landSpeed;
        pulseColor1 = landColor1;
        pulseColor2 = landColor2;
		EmitSound(numPulsesPerLand, maxLandFovDegrees, landSpeedMultIncrementBetweenPulses, Vector3.up);
	}
}
