﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundPulse : MonoBehaviour 
{
	private const float lineWallOffset = 0.05f; // To avoid z-fighting

    public float lineTremble;
    public float lineDistanceTremble;
    public float randomDir;
    public float maxEmissionDistance = 999999.9f; 

	public float lifeTimeAfterCollided;
    public float fadeOutPercent;

    public Color color;

    // Rays casted in a single pulse in every concentric direction
    public int raysPerPulse;
    public bool ignoreSelfCollisions;
    public bool seeLinesInAir;

    // Speed of propagation (in fact, this is the increment speed of the raycast maxDist)
    public float speed;  

    // Raycast distance that will grow as the pulse goes on. 
    // This will give the feel of the wave being moved
    private float raycastDist; 

    // List of the points belonging to the original emission unit sphere. 
    // From these we calculate the direction of our raycasts.
    private Vector3 emissionOrigin; // Where the sound has started.
    private Vector3 emissionDir; // The line that will "follow" the emission (the "axis").
    private float fov; // fov with respect to the SoundEmitter that creates this pulse
    public float fovSpeed = 0.001f;

    // Every direction of the ray that must be cast from the emissionOrigin.
    // This is calculated once in the Init function
    private List<Vector3> rayDirs;
    public List<Vertex> vertices; // One for each ray. We will move them as we raycast every frame

    private float timeSinceEmitted = 0.0f;
    private int verticesNotColliding = 0;
    private SoundEmitter parentEmitter;

    private LineStripDrawGL lineDrawer;

    private float radius = 1.0f;

    private void GenerateDirections ()
    {
        rayDirs = new List<Vector3>();

        Quaternion lookRot = Quaternion.LookRotation(emissionDir);
        float sinFov = Mathf.Sin(fov), cosFov = Mathf.Cos(fov);

        float step = Mathf.PI / raysPerPulse;
        for (float phi = 0.0f; phi < 2 * Mathf.PI; phi += step)
        {
            Vector3 dir = new Vector3(Mathf.Cos(phi) * sinFov, Mathf.Sin(phi) * sinFov, cosFov);

            // Look towards the emissionDir
            dir = lookRot * dir; 
            if (lineTremble > 0.0f) dir += Random.insideUnitSphere * lineTremble;

            rayDirs.Add(dir.normalized);
        }

        rayDirs.Add(rayDirs[0]); // Loop
    }

    public void Init (float fov, Vector3 emissionOrigin, Vector3 emissionDir, SoundEmitter parentEmitter)
    {
        this.emissionOrigin = emissionOrigin;
        this.emissionDir = emissionDir;
        this.fov = fov;
        this.raycastDist = 0.0f;

        this.parentEmitter = parentEmitter;
        timeSinceEmitted = 0.0f;

        // Generate the direction of the rays corresponding to the pulse
        GenerateDirections();
        vertices = new List<Vertex>(); // As many vertices as rays
        for (int i = 0; i < rayDirs.Count; ++i)
        {
            vertices.Add(new Vertex());
        }
    }

	void Start () 
    {
        lineDrawer = GetComponentInChildren<LineStripDrawGL>();
	}

	void Update () 
    {
        if (rayDirs == null) return;

        // Must we destroy this pulse?
        bool mustDestroy = true;
        foreach (Vertex v in vertices)
        {
            if (!v.collided && raycastDist < maxEmissionDistance)
            {
                mustDestroy = false;
                break;
            }
            else if (v.collided && v.GetTimeSinceCollided() < lifeTimeAfterCollided)
            {
                mustDestroy = false;
                break;
            }
        }

        if (vertices.Count > 0 && mustDestroy)
        {
            DestroyPulse();
            return;
        }
        //

        fov += fovSpeed * Time.deltaTime;
        GenerateDirections();

        timeSinceEmitted += Time.deltaTime;
        bool maxRadiusReached = raycastDist >= maxEmissionDistance;
        raycastDist += speed * Time.deltaTime;
        raycastDist = Mathf.Min(raycastDist, maxEmissionDistance);

        for (int i = 0; i < rayDirs.Count; ++i)
        {
            vertices[i].Update();
        }
        
        for (int i = 0; i < rayDirs.Count; ++i)
        {
            Vertex v = vertices[i];
            v.hide = false;

            if (v.collided)
            {
                // No need to recast. Performance optimization. Comment this if you want dynamic wrapping around obstacles.
                // continue; 
            }

            Vector3 dir = rayDirs[i];

            bool hitted = false;
            RaycastHit[] hits = new RaycastHit[0];
            Ray r = new Ray(emissionOrigin, dir);
            if (ignoreSelfCollisions)
            {
                hits = Physics.RaycastAll(r, raycastDist);
                hitted = hits.Length > 0;
            }
            else
            {
                RaycastHit hitInfo;
                hitted = Physics.Raycast(r, out hitInfo, raycastDist);
                if (hitted)
                {
                    hits = new RaycastHit[1];
                    hits[0] = hitInfo;
                }
            }

            RaycastHit closestHit = new RaycastHit(); // Get the closest hit
            closestHit.distance = 99999.9f;
            if (hitted)
            {
                hitted = false;
                foreach (RaycastHit h in hits)
                {
                    if (h.distance < closestHit.distance)
                    {
                        if (ignoreSelfCollisions)
                        {
                            if (h.collider.gameObject != parentEmitter.gameObject)
                            {
                                closestHit = h;
                                hitted = true;
                            }
                        }
                        else
                        {
                            closestHit = h;
                            hitted = true;
                        }
                    }
                }

                hitted = closestHit.distance != 99999.9f;
            }

            if (hitted)
            {
                v.position = closestHit.point + closestHit  .normal * lineWallOffset;
                v.normal = closestHit.normal;
                v.collided = true;
            }
            else
            {
                float randomRaycastDist = raycastDist + raycastDist * lineDistanceTremble * Random.value;
                       
                Vector3 rayFinalPosition = emissionOrigin + r.direction * randomRaycastDist;
                v.position = rayFinalPosition;
                v.normal = (rayFinalPosition - emissionOrigin).normalized;
                v.collided = false;
            }
        }

        if (!seeLinesInAir) FilterAirLines();
        FilterLongLinesRelativeLength();
        UpdateDrawDataOfPulseLine(color);
    }

    // If a line is much longer than its neighbours, its considered to be very long
    private void FilterLongLinesRelativeLength()
    {
        for (int i = 0; i < vertices.Count; ++i)
        {
            Vertex vleft2 = vertices[GetCircularVertexIndex(i - 2)];
            Vertex vleft  = vertices[GetCircularVertexIndex(i - 1)];
            Vertex v      = vertices[i];
            Vertex vright = vertices[GetCircularVertexIndex(i + 1)];

            float dleft  = Vector3.SqrMagnitude(vleft2.position - vleft.position);
            float d      = Vector3.SqrMagnitude(vleft.position - v.position);
            float dright = Vector3.SqrMagnitude(v.position - vright.position);

            float neighbourDistMean = (dleft + dright) * 0.5f;
            v.hide = (d >= neighbourDistMean * 3.0f) || v.hide;
        }
    }

    // I take profit of veryLong property here :p
    private void FilterAirLines()
    {
        for (int i = 0; i < vertices.Count; ++i)
        {
            Vertex v0 = vertices[GetCircularVertexIndex(i - 1)];
            Vertex v1 = vertices[i];

            v1.hide = (!v0.collided && !v1.collided) || v1.hide;
        }
    }
    private void UpdateDrawDataOfPulseLine (Color color)
    {
        // Propagate veryLong
        for (int i = 0; i < vertices.Count; ++i)
        {
            if (vertices[i].hide)
            {
                vertices[GetCircularVertexIndex(i - 1)].hide = true;
            }
        }
        //

        lineDrawer.Clear();
        foreach (Vertex v in vertices)
        {
            v.color = color;

            // Determine alpha
            if (v.hide)
            {
                v.color.a = 0.0f;
            }
            else
            {
                if (!v.collided)
                { 
                    // The closer to the max radius, the less alpha.
                    v.color.a = 1.0f - (raycastDist / maxEmissionDistance);
                }
                else // Vertex Collided
                {
                    v.color.a = 1.0f - (v.GetTimeSinceCollided() / lifeTimeAfterCollided);
                }
            }

            lineDrawer.AddVertex(v);
        }
    }

    private void DestroyPulse ()
    {
        parentEmitter.RemovePulse(this);
        Destroy(gameObject);
    }

    public void SetEmissionOrigin (Vector3 emissionOrigin)
    {
        this.emissionOrigin = emissionOrigin;
    }

    private int GetCircularVertexIndex(int i)
    {
        return (i + vertices.Count) % vertices.Count;
    }
}
