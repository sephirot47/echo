﻿using UnityEngine;
using System.Collections;

public class DrawLinesCaller : MonoBehaviour 
{
    void OnPostRender()
    {
        GameObject[] lineDrawers = GameObject.FindGameObjectsWithTag("LineDrawer");
        foreach (GameObject ld in lineDrawers)
        {
            ld.GetComponent<LineStripDrawGL>().OnPostRender();
        }
    }
}
