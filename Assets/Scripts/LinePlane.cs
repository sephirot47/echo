﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LinePlane : MonoBehaviour 
{
	public Color color;
	public int numLines = 2;
	public int numLineSegments = 8;
	public Vector3 lineTremble = Vector3.zero;
	public GameObject lineDrawerPrefab;

	private List<LineStripDrawGL> lineDrawers;

	void Start () 
	{
		lineDrawers = new List<LineStripDrawGL> ();

		for (int i = 0; i < numLines; ++i) 
		{
			LineStripDrawGL lineDrawer = 
				( GameObject.Instantiate(lineDrawerPrefab) as GameObject).GetComponent<LineStripDrawGL>();
			lineDrawer.transform.parent = this.transform;
			lineDrawers.Add (lineDrawer);
		}
	}

	Vertex[] GetVertices(int lineIndex)
	{
		Vertex[] result = new Vertex[numLineSegments];

		float halfWidth = 5.0f;
		float wStep = (halfWidth * 2.0f) / (numLines == 1 ? 1.0f : numLines-1);
		float w = -halfWidth + wStep * lineIndex;

		float halfDepth = 5.0f;
		float dStep = (halfDepth * 2.0f) / (numLineSegments == 1 ? 1.0f : numLineSegments-1);
		int i = 0;
		for(float d = -halfDepth; d <= halfDepth; d += dStep)
		{
			Vector3 incr = new Vector3 (w, 0,  d);
			incr = transform.TransformVector(incr);

			Vector3 rand = transform.TransformVector(Random.value * lineTremble);
			Vector3 pos = transform.position + incr + rand;

			result[i] = new Vertex (pos, Vector3.one, color);
			++i;
		}

		return result;
	}

	void Update () 
	{
		int i = 0;
		foreach (LineStripDrawGL lineDrawer in lineDrawers) 
		{
			lineDrawer.Clear ();
			Vertex[] vs = GetVertices(i);
			for (int j = 0; j < numLineSegments; ++j) 
			{
				lineDrawer.AddVertex (vs[j]);
			}
			++i;
		}
	}

	void OnDrawGizmos()
	{
		Gizmos.color = color;

		for(int i = 0; i < numLines; ++i)
		{
			Vertex[] vs = GetVertices(i);
			for (int j = 1; j < numLineSegments; ++j) 
			{
				Gizmos.DrawLine(vs[j-1].position, vs[j].position);
			}
		}
	}
}
