﻿using UnityEngine;
using System.Collections;

public class Enemy : SoundEmitter
{
    [Header("Enemy")]
    public float movementSpeed;
    public float changeSpeedTime;

    private Vector3 movement;
    private float time1 = 0.0f;
    private float time2 = 0.0f;

    public float secondsBetweenSounds;

    Rigidbody rb;

    void Start()
    {
        base.Start();

        rb = GetComponent<Rigidbody>();
        ChangeMovement();
		EmitOmniDirectionalSound();
    }

	void Update () 
    {
        base.Update();
        
        time1 += Time.deltaTime;
        time2 += Time.deltaTime;
        if(time1 > changeSpeedTime)
        {
            time1 = 0.0f;
            ChangeMovement();
        }

        if (time2 > secondsBetweenSounds)
        {
            time2 = 0.0f;
			EmitOmniDirectionalSound();
        }

        rb.velocity = movement;
    }

    void ChangeMovement()
    {
        Vector3 dir = Random.insideUnitSphere;
        movement = new Vector3(dir.x, 0.0f, dir.z) * movementSpeed;
    }

    void OnCollisionEnter(Collision col)
    {
        ChangeMovement();
    }

	void OnDrawGizmos()
	{
        base.OnDrawGizmos();
		Gizmos.DrawWireSphere (transform.position, pulseMaxEmissionDistance);
	}
}
