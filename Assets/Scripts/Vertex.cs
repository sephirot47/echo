﻿using UnityEngine;
using System.Collections;

public class Vertex
{
    public Vector3 position, normal;
    public Color color;
    public bool collided;
    public bool hide;

    private float timeSinceEmitted;
    private float timeSinceCollided;

    public Vertex(Vector3 pos, Vector3 norm, Color col)
    {
        position = pos;
        normal = norm;
        color = col;
        collided = false;
        hide = false;
        timeSinceCollided = 0.0f;
    }

    public Vertex()
    {
        position = Vector3.zero;
        normal = Vector3.zero;
        color = Color.black;
        collided = false;
        timeSinceCollided = 0.0f;
    }

    public Vertex(Vector3 pos)
    {
        position = pos;
        normal = Vector3.zero;
        color = Color.black;
        collided = false;
        timeSinceCollided = 0.0f;
    }


    public void Update()
    {
        if (collided)
        {
            timeSinceCollided += Time.deltaTime;
        }

        timeSinceEmitted += Time.deltaTime;
    }

    public float GetTimeSinceEmitted()
    {
        return timeSinceEmitted;
    }

    public float GetTimeSinceCollided()
    {
        return timeSinceCollided;
    }
}
