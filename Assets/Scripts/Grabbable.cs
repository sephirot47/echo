﻿using UnityEngine;
using System.Collections;

public class Grabbable : MonoBehaviour
{
    public Material onGrabbedMaterial;
    private Material normalMaterial;

    bool grabbed = true;

	void Start ()
    {
        normalMaterial = GetComponent<Renderer>().material;
    }
	
	void Update ()
    {
    }
    
    public void OnGrabbed()
    {
        GetComponent<Renderer>().material = onGrabbedMaterial;
        GetComponent<Rigidbody>().useGravity = false;
    }

    public void OnDropped()
    {
        GetComponent<Renderer>().material = normalMaterial;
        GetComponent<Rigidbody>().useGravity = true;
    }

    public void OnCollisionEnter(Collision col)
    {
        if (!col.gameObject.CompareTag("Player"))
        {
            SoundEmitter se = GetComponent<SoundEmitter>();
            if (se)
            {
                Rigidbody rb = se.GetComponent<Rigidbody>();

                float vel = col.impulse.magnitude * rb.mass; //rb.velocity.magnitude;
                se.pulseSpeed = vel;
                se.pulseMaxEmissionDistance = vel;

                Vector3 normalMean = Vector3.zero;
                foreach (ContactPoint c in col.contacts)
                {
                    normalMean += c.normal;
                }
                normalMean /= col.contacts.Length;

                se.EmitSound(se.numPulses, 180, 0.0f, normalMean);
            }
        }

    }

    void OnDrawGizmos()
    {
        Gizmos.DrawIcon(transform.position, "hand", true);
    }
}
