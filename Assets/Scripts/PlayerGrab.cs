﻿using UnityEngine;
using System.Collections;

public class PlayerGrab : MonoBehaviour
{
    public float maxGrabDistance = 50.0f;
    public float moveSpeed = 5.0f;
    public float rotSpeed = 2.0f;
    public float throwForce = 10.0f;

    private float distanceWhenGrabbed = 0.0f;
    private Grabbable currentGrabbed = null;
    private Rigidbody currentGrabbedRB = null;

    private SpringJoint spring;

    void Start ()
    {
        spring = GetComponent<SpringJoint>();
    }
	
	void Update ()
    {
        if (currentGrabbed) // Currently grabbing something
        {
            HandleGrabbablePosition();
            HandleGrabbableRotation();

            if (Input.GetKeyDown(KeyCode.R))
            {
                ThrowGrabbable();
            }

            if (Input.GetKeyUp(KeyCode.Q)) // Ungrab
            {
                UnGrab();
            }
        }
        else // Look for something to grab
        {
            RaycastHit hitInfo;
            if (Physics.Raycast(transform.position, transform.forward, out hitInfo, maxGrabDistance))
            {
                Grabbable g = hitInfo.collider.gameObject.GetComponent<Grabbable>();
                if (g && Input.GetKeyDown(KeyCode.Q))
                {
                    currentGrabbed = g;
                    currentGrabbedRB = g.GetComponent<Rigidbody>();
                    Physics.IgnoreCollision(transform.parent.GetComponent<Collider>(),
                                            currentGrabbed.GetComponent<Collider>());
                    distanceWhenGrabbed = Vector3.Distance(hitInfo.point, transform.position);
                    g.OnGrabbed();
                }
            }
        }
	}

    void UnGrab()
    {
        Physics.IgnoreCollision(transform.parent.GetComponent<Collider>(),
                                currentGrabbed.GetComponent<Collider>(), false);
        currentGrabbed.OnDropped();
        currentGrabbed = null;
        currentGrabbedRB = null;
    }

    void HandleGrabbablePosition ()
    {
        RaycastHit hitInfo;

        // Limit how far the grabbable can go from the grab origin. If we dont do this the grabbable
        // can go beyond walls or other obstacles
        float maxDistance = 9999.0f; // The maximum dist the grabbable can be from the grab origin
        int layerIgnoreGrabbable = 1 << LayerMask.NameToLayer("Grabbable");
        layerIgnoreGrabbable |= 1 << LayerMask.NameToLayer("Ignore Raycast");
        if (Physics.Raycast(transform.position, transform.forward,
                            out hitInfo, distanceWhenGrabbed, ~layerIgnoreGrabbable))
        {
            maxDistance = hitInfo.distance * 0.9f;
        }

        float d = Mathf.Min(distanceWhenGrabbed, maxDistance);
        Vector3 destinyPosition = transform.position + transform.forward * d;
        Vector3 pos = Vector3.Lerp(currentGrabbed.transform.position, destinyPosition,
                                   moveSpeed * Time.deltaTime);
        currentGrabbedRB.MovePosition(pos);
    }

    void HandleGrabbableRotation()
    {
        Quaternion destinyRot = Quaternion.LookRotation(transform.forward);
        Quaternion rot = Quaternion.Lerp(currentGrabbed.transform.rotation, destinyRot,
                                         rotSpeed * Time.deltaTime);
        currentGrabbed.transform.rotation = rot;
    }

    void ThrowGrabbable()
    {
        currentGrabbedRB.AddForce(transform.forward * throwForce, ForceMode.Impulse);
        UnGrab();
    }
}
