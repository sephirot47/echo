﻿using UnityEngine;
using System.Collections;

public class AutoDestroy : MonoBehaviour 
{
    public float lifeTime = 1.0f;
	void Start () 
    {
        Destroy(gameObject, lifeTime);
	}
}
