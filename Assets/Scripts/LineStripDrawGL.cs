using UnityEngine;
using System.Collections.Generic;

public class LineStripDrawGL : MonoBehaviour 
{
	public Shader shader;
    public Material material;

    bool firstVertexAdded = false;
    Vertex lastVertexAdded;
    Vertex[] verticesBuffer;
    int lastVertexIndex;

	void Start () 
    {
        material = new Material(material); // Make a copy of the material
        material.hideFlags = HideFlags.HideAndDontSave;

        verticesBuffer = new Vertex[10000];
		Clear();
	}

	public void AddVertex(Vertex worldVertex)
    {
        if (firstVertexAdded)
        {
            verticesBuffer[lastVertexIndex] = lastVertexAdded;
            verticesBuffer[lastVertexIndex + 1] = lastVertexAdded = worldVertex;
            lastVertexIndex += 2;
        }
        else
        {
            firstVertexAdded = true;
            lastVertexAdded = worldVertex;
        }
	}

    public void OnPostRender()
    {
        GL.PushMatrix();
        GL.Begin( GL.LINES );
        material.SetPass(0);

        for(int i = 0; i < lastVertexIndex; ++i)
		{
            GL.Color (verticesBuffer[i].color);
			Vector3 v = verticesBuffer[i].position;
            GL.Vertex3(v.x, v.y, v.z);
        }

        GL.End();
        GL.PopMatrix();
    } 

    public Material GetMaterial()
    {
        return material;
    }

	public void Clear()
	{
        firstVertexAdded = false;
		lastVertexIndex = 0;
		if (material == null) 
		{
            material = new Material (shader);
		}
	}

    /*
    public void SetNumPoints(int num)
    {
        verticesBuffer = new Vector3[num * 4];
    }
    */
}