﻿using UnityEngine;
using System.Collections;

public class PeriodicSoundEmitter : SoundEmitter
{
    [Header("Periodic SE")]
    public float timeBetweenEmissions;

	[Range (0.0f, 180.0f)]
	public float maxSoundFovDegrees = 45.0f;
	public float soundSpeedMultIncrementBetweenPulses = 0.5f;

    private float time = 1.0f;

    void Start()
    {
        base.Start();
		EmitSound(numPulses, maxSoundFovDegrees, soundSpeedMultIncrementBetweenPulses);
    }

	void Update () 
    {
        base.Update();
        
        time += Time.deltaTime;
        if(time > timeBetweenEmissions)
        {
			time = 0.0f;
			EmitSound(numPulses, maxSoundFovDegrees, soundSpeedMultIncrementBetweenPulses);
        }
	}

	void OnDrawGizmos()
	{
		base.OnDrawGizmos();
		Gizmos.DrawWireSphere (transform.position, pulseMaxEmissionDistance);
	}
}
