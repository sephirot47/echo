﻿using UnityEngine;
using System.Collections;

public class SonarShoot : SoundEmitter
{
    public Material sonarMaterial;

    // public float pulsesFov;
    public float propagationSpeed;
    public float propagationAmplitude;

    void Start ()
    {
        base.Start();
	}
	
	void Update ()
    {
        base.Update();

        if (Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit hitInfo;
            if (Physics.Raycast(transform.position, transform.forward, out hitInfo, 99.9f))
            {
                SonarizeObject(hitInfo.point, hitInfo.collider.gameObject);
            }
        }
	}

    public void SonarizeObject(Vector3 center, GameObject go)
    {
        Renderer r = go.GetComponent<Renderer>();
        r.material = new Material(sonarMaterial);
        r.material.SetColor("_SonarColor", Color.green);
        r.material.SetVector("_HitPoint", center);
        r.material.SetFloat("_InitTime",  Time.time);
        r.material.SetFloat("_PropSpeed", propagationSpeed);
        r.material.SetFloat("_PropAmpl",  propagationAmplitude);
    }
}
