Shader "Sound/Sonar" {
	
	Properties{
		[HideInInspector] _Color("Main Color", Color) = (0,1,0,1)
		[HideInInspector] _SonarColor("Sonar Color", Color) = (0,1,0,1)
		[HideInInspector] _HitPoint("Hit Point", Vector) = (0,0,0,0)
		[HideInInspector] _InitTime("Init Time", Float) = 0.0
		[HideInInspector] _PropSpeed("Propagation Speed", Float) = 1.0
		[HideInInspector] _PropAmpl("Propagation Amplitude", Float) = 1.0
		[NoScaleOffset]   _SonarTexture("Sonar Texture", 2D) = "white" {}
	}

		SubShader{
		Pass {
			ZTest LEqual
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite On
			Color[_Color]
			Lighting Off
			Cull Off
			Fog { Mode Off }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			fixed4 _SonarColor;
			fixed4 _HitPoint;
			float _InitTime;
			float _PropSpeed;
			float _PropAmpl;
			sampler2D _SonarTexture;

            struct VertexIn
            {
                float4 vertex : POSITION;
                float4 color : COLOR0;
                float2 uv : TEXCOORD0;
            };

            struct FragmentIn
            {
                float4 vertex : POSITION;
				float4 worldPosition : W_POSITION;
                float4 color : COLOR0;
                float2 uv : TEXCOORD0;
            };

            FragmentIn vert (VertexIn v)
            {
                FragmentIn o;
                o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.worldPosition = mul(_Object2World, v.vertex);
                o.color = v.color;
                o.uv = v.uv;
                return o;
            }
            
            fixed4 frag (FragmentIn fin) : SV_Target
            {
				float t = (_Time.y - _InitTime.x) * _PropSpeed.x + 1.0f;
			    float d = distance(_HitPoint, fin.worldPosition);

				fixed4 sc = tex2D(_SonarTexture, fin.uv);

				if (t - d > 0.0f && t - d < _PropAmpl.x) // Circle in
				{
					//sc *= 1.0f - ((t - d) / _PropAmpl.x * 2.0f);
					sc *= d / t;
				}
				else if (d - t > 0.0f && d - t < _PropAmpl.x) // Circle out
				{
					sc *= 1.0f - ((d - t) / _PropAmpl.x * 5.0f);
				}
				else // Not in circle outline
				{
					if(d < t)  // Inside circle
					{
						sc *= d / t;
					}
					else  // Outside circle
					{
						sc = fixed4(0,0,0,0);
					}
				}

				return fixed4(sc.x, sc.y, sc.z, 1);
            }

            ENDCG
        }
    }
    
    FallBack "VertexLit"
} 