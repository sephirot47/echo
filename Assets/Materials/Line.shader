Shader "Lines/Line" {
	
    Properties {
    	_Color ("Main Color", Color) = (0,1,0,1)
    }
    
    SubShader {
        Pass {
            ZTest LEqual
        	Blend SrcAlpha OneMinusSrcAlpha 
            ZWrite Off
        	Color [_Color]
            Lighting Off
            Cull Off
            Fog { Mode Off }

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct VertexIn
            {
                float4 vertex : POSITION;
                float4 color : COLOR0;
            };

            struct FragmentIn
            {
                float4 vertex : SV_POSITION;
                float4 color : COLOR0;
            };

            FragmentIn vert (VertexIn v)
            {
                FragmentIn o;
                o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
                o.color = v.color;
                return o;
            }
            
            fixed4 frag (FragmentIn fin) : SV_Target
            {
                return fin.color;
            }
            ENDCG
        }
    }
    
    FallBack "VertexLit"
} 