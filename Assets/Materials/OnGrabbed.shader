Shader "Sound/OnGrabbed" {
	
	Properties{
		_Color("Main Color", Color) = (0,1,0,1)
		_BlinkSpeed("BlinkSpeed", Float) = 1.0
		[HideInInspector] _Center("Center", Vector) = (0,0,0,0)
	}

		SubShader{
		Pass {
			ZTest LEqual
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite On
			Color[_Color]
			Lighting Off
			Cull Off
			Fog { Mode Off }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			fixed4 _Color;
			float _BlinkSpeed;
			fixed4 _Center;

            struct VertexIn
            {
                float4 vertex : POSITION;
                float4 color : COLOR0;
            };

            struct FragmentIn
            {
                float4 vertex : SV_POSITION;
				float4 worldPosition : W_POSITION;
                float4 color : COLOR0;
            };

            FragmentIn vert (VertexIn v)
            {
                FragmentIn o;
                o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.worldPosition = mul(_Object2World, v.vertex);
                o.color = v.color;
                return o;
            }
            
            fixed4 frag (FragmentIn fin) : SV_Target
            {
				float t = sin(_Time.y * _BlinkSpeed);
				fixed4 c = _Color * (t * 0.5f + 0.5f + 0.2f);
				return fixed4(c.x, c.y, c.z, 1);
            }

            ENDCG
        }
    }
    
    FallBack "VertexLit"
} 